// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the staggered grid multi-component (Navier-)Stokes model
 */
#include <config.h>
#include <ctime>
#include <memory>
#include <iostream>
#include <sstream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>
#include <dumux/geometry/diameter.hh>

#include "densityflowproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>


#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>


int main(int argc, char** argv) try
{
    using namespace Dumux;
    // define the type tag for this problem
    using TypeTag = Properties::TTag::DensityDrivenFlowProblem;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();
    
    //the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");

    // instantiate time loop
    //auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    if (hasParam("TimeLoop.EpisodeLength"))
        timeLoop->setPeriodicCheckPoint(getParam<Scalar>("TimeLoop.EpisodeLength"));

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = leafGridView.size(1);
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);
    problem->applyInitialSolution(x);
    problem->setTimeLoop(timeLoop);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView; 
    using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimensionworld>;

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //! Add model specific output fields
    vtkWriter.addField(problem->getDeltaRho(), "deltaRho");
    //vtkWriter.addField(problem->getpH(), "pH");
    //vtkWriter.addField(problem->getmCa(), "mCa");
    //vtkWriter.addField(problem->getmTIC(), "mTIC");
    //vtkWriter.addField(problem->getmCO3(), "mCO3");
    //vtkWriter.addField(problem->getrdiss(), "rdiss");
    //vtkWriter.addField(problem->getOmega(), "Omega");

    std::vector<Scalar> pecletNumber(numDofsCellCenter);
    vtkWriter.addField(pecletNumber, "Pe");
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    FluxOverSurface<GridVariables,
                SolutionVector,
                GetPropType<TypeTag, Properties::ModelTraits>,
                GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);

     if constexpr (GridView::dimension == 2)
    {
        using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimensionworld>;
        const auto lowerLeftCorner = gridGeometry->bBoxMin();
        const auto upperLeftCorner = GlobalPosition{gridGeometry->bBoxMin()[0], gridGeometry->bBoxMax()[1]};
        const auto lowerRightCorner =  GlobalPosition{gridGeometry->bBoxMax()[0], gridGeometry->bBoxMin()[1]};
        const auto UpperRightCorner = gridGeometry->bBoxMax();

        // flux over upper boundary
        flux.addSurface("upperBoundary", upperLeftCorner, UpperRightCorner);

        // flux over left boundary
        flux.addSurface("leftBoundary", lowerLeftCorner, upperLeftCorner);

        // flux over left boundary
        flux.addSurface("rightBoundary", lowerRightCorner, UpperRightCorner);
    }

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    std::ofstream fileout;
    if (GridView::dimension == 2)
    {
        fileout.open ("flux.txt");
        fileout << "time  fluxUpWater  fluxUpCO2  fluxUpCa  fluxLeftWater  fluxLeftCO2  fluxLeftCa  fluxRightWater fluxRightCO2  fluxRightCa" << std::endl;
    }
    
    //Scalar normTime;
    //bool normTimeFlag {true};
    // time loop
    timeLoop->start(); do
    {   
        // update pH values 
        //problem->setValues(*gridVariables, x); //auskommentiert

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);
        
        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        if (!hasParam("TimeLoop.EpisodeLength") || timeLoop->isCheckPoint() || timeLoop->finished() || timeLoop->timeStepIndex() == 1)
        {
          problem->calculateDeltaRho(*gridVariables, x);
          problem->calculateTotalCO2Mass(*gridVariables, x);
	    }
        
        //// entries for csv file
        //if (problem->getpH()[wallIdx] >= 7.0 && normTimeFlag){
            //normTime = time;
            //normTimeFlag = false;
        //}
//
        //// mid boundary value to the grid (at the mid wall) was used to save the values in the csv file
        //fout << time << ",";
        //fout << problem->getpH()[wallIdx] << ",";
        //fout << problem->getmCa()[wallIdx] << ",";
        //fout << problem->getmTIC()[wallIdx] << ",";
        //fout << problem->getmCO3()[wallIdx] << ",";
        //fout << problem->getrdiss()[wallIdx] << ",";
        //fout << problem->getOmega()[wallIdx] << "\n";
//
        //for (const auto& element : elements(leafGridView))
        //{
            //auto fvGeometry = localView(*gridGeometry);
            //auto elemFaceVars = localView(gridVariables->curGridFaceVars());
            //fvGeometry.bindElement(element);
            //elemFaceVars.bindElement(element, fvGeometry, x);
//
            ////GlobalPosition velocity(0.0);
//
            //for (auto&& scv : scvs(fvGeometry))
            //{
                //auto dofIdxGlobal = scv.dofIndex();
//
                //for (auto&& scvf : scvfs(fvGeometry))
                //{
                    //auto dirIdx = scvf.directionIndex();
                    //velocity[dirIdx] += 0.5*elemFaceVars[scvf].velocitySelf();
                //}
//
                //const Scalar velMag = velocity.two_norm();
                //pecletNumber[dofIdxGlobal] = velMag * diameter(element.geometry()) / 2e-9;
            //}
        //}

        // write vtk output
	// if episode length was specificied output only at the end of episodes
        if (!hasParam("TimeLoop.EpisodeLength") || timeLoop->isCheckPoint() || timeLoop->finished() || timeLoop->timeStepIndex() == 1)
            vtkWriter.write(timeLoop->time());

	    if constexpr(GridView::dimension == 2)
        {
            flux.calculateMassOrMoleFluxes();

            std::ostream tmpOutputObject(std::cout.rdbuf()); // create temporary output with fixed formatting without affecting std::cout
            tmpOutputObject << "fluxUpperBoundary: " << std::setprecision(8) << std::scientific << flux.netFlux("upperBoundary") << std::endl;
            tmpOutputObject << "fluxLeftBoundary: " << std::setprecision(8) << std::scientific << flux.netFlux("leftBoundary") << std::endl;
            tmpOutputObject << "fluxRightBoundary: " << std::setprecision(8) << std::scientific << flux.netFlux("rightBoundary") << std::endl;
            fileout << std::setprecision(8) << std::scientific
                                            << timeLoop->time() << " "
                                            << flux.netFlux("upperBoundary") << " "
                                            << flux.netFlux("leftBoundary") << " "
                                            << flux.netFlux("rightBoundary") << std::endl;
        }


        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

        
    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());
    //fout.close();

    //std::ifstream fin{"tableDUMUX.csv"};
    //std::string line;
    //if (!fin){
        //std::cerr << "Problem opening table.csv file" << std::endl;
        //return 1;
    //}
    //Scalar temp;
//
    //while(std::getline(fin, line)){
        //int colCount = 0;
        //std::istringstream iss{line};
//
        //while(iss >> temp){
            //if(iss.peek() == ',') iss.ignore();
            //if (colCount == 0){
                //fnormout << temp/normTime << ",";
                //++colCount;
            //}
            //else{
                //fnormout << temp << ",";
                //++colCount;
            //}
            //if (colCount == 7)
                //fnormout << "\n";
        //}
    //}


    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }
    //fnormout.close();
    //fin.close();
    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
