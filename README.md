This is the DuMuX module containing the code for producing the results of 

Class, H., Bürkle, P., Trötschler, O., Zimmer, M., Strauch, B.,
On the role of density-driven dissolution of CO2 in karstification
2021

Installation
============

The easiest way to install this module and its dependencies is to create a new directory

```shell
mkdir DUMUX-DENSITYDISSOLUTION && cd DUMUX-DENSITYDISSOLUTION
```

and download the install script

[installscript.sh](https://git.iws.uni-stuttgart.de/dumux-pub/buerkle2021a/-/blob/master/installscript.sh)

to that folder and run the script with

```shell
chmod u+x installscript.sh
./installscript.sh
```

go into directory and run the column or flume application (for the flume appl. replace "column" with "flume"), 
"./column" starts the simulation, in "params.input" changes can be made 
```shell
cd buerkle2021a/build-cmake/appl/column/
make column
./column
```
To view the results, paraview must be used.

Installation with Docker 
========================

Create a new folder in your favourite location and change into it

```bash
mkdir DUMUX-DENSITYDISSOLUTION && cd DUMUX-DENSITYDISSOLUTION
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/buerkle2021a/-/raw/master/docker/docker_densitydrivendissolution.sh
```

Open the Docker Container
```bash
bash docker_densitydrivendissolution.sh open
```

After the script has run successfully, you may go into directory and run the column or flume application (for the flume appl. replace "column" with "flume"), "./column" starts the simulation, in "params.input" changes can be made 

```bash
cd buerkle2021a/build-cmake/appl/column/
make column
./column
```
To view the results, paraview must be used.
